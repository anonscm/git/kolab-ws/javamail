<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copy of javax.mail, patched to still build,
    plus behavioural and API-relevant changes necessary for kolab-ws
-->
<!--

    DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.

    Copyright (c) 1997-2015 Oracle and/or its affiliates. All rights reserved.

    The contents of this file are subject to the terms of either the GNU
    General Public License Version 2 only ("GPL") or the Common Development
    and Distribution License("CDDL") (collectively, the "License").  You
    may not use this file except in compliance with the License.  You can
    obtain a copy of the License at
    https://glassfish.dev.java.net/public/CDDL+GPL_1_1.html
    or packager/legal/LICENSE.txt.  See the License for the specific
    language governing permissions and limitations under the License.

    When distributing the software, include this License Header Notice in each
    file and include the License file at packager/legal/LICENSE.txt.

    GPL Classpath Exception:
    Oracle designates this particular file as subject to the "Classpath"
    exception as provided by Oracle in the GPL Version 2 section of the License
    file that accompanied this code.

    Modifications:
    If applicable, add the following below the License Header, with the fields
    enclosed by brackets [] replaced by your own identifying information:
    "Portions Copyright [year] [name of copyright owner]"

    Contributor(s):
    If you wish your version of this file to be governed by only the CDDL or
    only the GPL Version 2, indicate your decision by adding "[Contributor]
    elects to include this software in this distribution under the [CDDL or GPL
    Version 2] license."  If you don't indicate a single choice of license, a
    recipient has the option to distribute your version of this file under
    either the CDDL, the GPL Version 2 or to extend the choice of license to
    its licensees as provided above.  However, if you add GPL Version 2 code
    and therefore, elected the GPL Version 2 license, then the option applies
    only if the new code is made subject to such option by the copyright
    holder.

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<parent>
		<groupId>com.sun.mail</groupId>
		<artifactId>all</artifactId>
		<version>1.5.3</version>
		<relativePath />
	</parent>
	<modelVersion>4.0.0</modelVersion>
	<artifactId>javax.mail</artifactId>
	<version>1.5.3.tarent.8-SNAPSHOT</version>
	<packaging>jar</packaging>
	<name>JavaMail API</name>
	<description>Patched JavaMail API for the ⮡ tarent Kolab Webservive</description>
	<organization>
		<name>tarent solutions GmbH, Germany</name>
		<url>http://www.tarent.de/</url>
	</organization>
	<properties>
		<mail.extensionName>javax.mail</mail.extensionName>
		<mail.specificationTitle>JavaMail(TM) API Design Specification</mail.specificationTitle>
		<mail.implementationTitle>javax.mail</mail.implementationTitle>
		<mail.packages.export>
			javax.mail.*; version=${mail.spec.version},
			com.sun.mail.imap; version=${mail.osgiversion},
			com.sun.mail.imap.protocol; version=${mail.osgiversion},
			com.sun.mail.iap; version=${mail.osgiversion},
			com.sun.mail.pop3; version=${mail.osgiversion},
			com.sun.mail.smtp; version=${mail.osgiversion},
			com.sun.mail.util; version=${mail.osgiversion},
			com.sun.mail.util.logging; version=${mail.osgiversion},
			com.sun.mail.handlers; version=${mail.osgiversion}
		</mail.packages.export>
		<mail.probeFile>META-INF/gfprobe-provider.xml</mail.probeFile>
		<findbugs.skip>true</findbugs.skip>
		<findbugs.exclude>${project.basedir}/exclude.xml</findbugs.exclude>
		<!-- version numbers -->
		<junit.version>4.7</junit.version>
		<!-- maven plugins -->
		<maven.deploy.version>2.8.2</maven.deploy.version>
		<maven.enforcer.version>1.4.1</maven.enforcer.version>
		<maven.gpg.version>1.1</maven.gpg.version>
		<maven.release.version>2.5.3</maven.release.version>
		<!-- minimum we can still build, should be 5 really -->
		<javaRelease>6</javaRelease>
	</properties>
	<build>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
			</resource>
		</resources>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-javadoc-plugin</artifactId>
					<version>3.0.1</version>
					<configuration>
						<additionalOptions>-html4 ${doclint.optappend}</additionalOptions>
						<javadocExecutable>/usr/bin</javadocExecutable>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-enforcer-plugin</artifactId>
				<version>${maven.enforcer.version}</version>
				<executions>
					<execution>
						<goals>
							<goal>enforce</goal>
						</goals>
						<configuration>
							<rules>
								<!-- Avoid the M.A.D. Gadget vulnerability in certain apache commons-collections versions -->
								<bannedDependencies>
									<excludes>
										<exclude>commons-collections:commons-collections:[3.0,3.2.1]</exclude>
										<exclude>commons-collections:commons-collections:4.0</exclude>
									</excludes>
								</bannedDependencies>
							</rules>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-release-plugin</artifactId>
				<version>${maven.release.version}</version>
				<executions>
					<execution>
						<id>default</id>
						<goals>
							<goal>perform</goal>
						</goals>
						<configuration>
							<pomFileName>javamail/pom.xml</pomFileName>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-gpg-plugin</artifactId>
				<version>${maven.gpg.version}</version>
				<executions>
					<execution>
						<id>sign-artifacts</id>
						<phase>none</phase>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-deploy-plugin</artifactId>
				<version>${maven.deploy.version}</version>
			</plugin>
			<!-- Configure compiler plugin to print lint warnings. -->
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.7.0</version>
				<executions>
					<execution>
						<id>default-compile</id>
						<configuration>
							<source>1.${javaRelease}</source>
							<target>1.${javaRelease}</target>
							<release>${javaRelease}</release>
							<encoding>${project.build.sourceEncoding}</encoding>
							<fork>true</fork>
							<compilerArgs>
								<arg>-Xlint:all</arg>
								<arg>-Xlint:-rawtypes</arg>
								<arg>-Xlint:-unchecked</arg>
								<arg>-Xlint:-finally</arg>
							</compilerArgs>
							<showWarnings>true</showWarnings>
						</configuration>
					</execution>
					<execution>
						<id>default-testCompile</id>
						<configuration>
							<source>1.${javaRelease}</source>
							<target>1.${javaRelease}</target>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<!-- Configure test plugin to find *TestSuite classes. -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<configuration>
					<includes>
						<include>**/*Test.java</include>
						<include>**/*TestSuite.java</include>
					</includes>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>exec-maven-plugin</artifactId>
				<version>1.4.0</version>
				<executions>
					<execution>
						<id>mksrc-mvnparent-build</id>
						<phase>compile</phase>
						<goals>
							<goal>exec</goal>
						</goals>
						<configuration>
							<executable>./src/main/ancillary/mksrc.sh</executable>
							<environmentVariables>
								<MKSRC_RUN_FROM_MAVEN>true</MKSRC_RUN_FROM_MAVEN>
							</environmentVariables>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>2.6</version>
				<configuration>
					<tarLongFileMode>posix</tarLongFileMode>
					<descriptors>
						<descriptor>src/main/ancillary/mksrc.xml</descriptor>
					</descriptors>
				</configuration>
				<executions>
					<execution>
						<id>mksrc-mvnparent-package</id>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
	<dependencies>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${junit.version}</version>
			<scope>test</scope>
			<optional>true</optional>
		</dependency>
	</dependencies>
	<scm>
		<url>https://evolvis.org/plugins/scmgit/cgi-bin/gitweb.cgi?p=kolab-ws/javamail.git</url>
		<connection>scm:git:https://evolvis.org/anonscm/git/kolab-ws/javamail.git</connection>
		<developerConnection>scm:git:ssh://maven@evolvis.org/scmrepos/git/kolab-ws/javamail.git</developerConnection>
		<tag>HEAD</tag>
	</scm>
	<distributionManagement>
		<repository>
			<id>tarent-nexus</id>
			<name>tarent-nexus releases</name>
			<url>https://repo-bn-01.lan.tarent.de/repository/maven-releases</url>
		</repository>
		<snapshotRepository>
			<id>tarent-nexus</id>
			<name>tarent-nexus snapshots</name>
			<url>https://repo-bn-01.lan.tarent.de/repository/maven-snapshots</url>
		</snapshotRepository>
	</distributionManagement>
</project>
